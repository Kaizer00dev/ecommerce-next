'use client';

import { Form, Input, Button } from 'antd';
import { useForm } from 'antd/es/form/Form';

export default function LoginPge() {
  const a = 'value';
  const [form] = Form.useForm();

  return (
    <div className="flex justify-center items-center h-screen">
      <div className="h-[500px] w-[500px] bg-gray-200 rounded-xl">
        <h1 className="text-center">Login</h1>
        <Form
          form={form}
          name="login"
          onFinish={(values) => {
            console.log(values);
          }}
        >
          <Form.Item label="Tên đăng nhập" name="username">
            <Input />
          </Form.Item>
          <Form.Item label="Mật khẩu" name="password">
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Đăng nhập
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
